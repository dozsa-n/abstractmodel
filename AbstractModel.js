/**
 * https://www.sqlite.org/pragma.html
 */
var squel = require('./squel.js');
var _ = require('./underscore-min.js');

var PRAGMA = {
	full_column_names: false,
	// short_column_names: false,
	encoding: 'UTF-8'
};

function debug(agrs){
	console.log(agrs);
}

function AbstractModel(){
	this.id = null;
	this.squel = squel;
	this.useDbConfig = 'default';
	this.alias = this.constructor.arguments.callee.name;
	this.useTable = null;
	this.tablePrefix = null;
	this.primaryKey = '_rowid_';
	// this.options = {};
	this.virtualFields = [];
	this._schema = [];

}

AbstractModel.prototype.setPragma = function(){
	var query = [];
	for(var prop in PRAGMA){
		query.push('PRAGMA '+prop+' = '+PRAGMA[prop]);
	}
	debug(query);
	return query;
};

AbstractModel.prototype.schema = function(){
	var sql = 'PRAGMA table_info('+this.useTable+')';
	debug(sql);
	return sql;
};

AbstractModel.prototype._tableName = function(){
	var from = new String();
	if(this.tablePrefix){
		from+= this.tablePrefix;
	}
	from+= this.useTable;
	return from;
}

AbstractModel.prototype._find = function(type){
	var self = this;
	var callers = {
		all: function () {
			var query = self.squel.select().from(self._tableName(), self.alias);
			return query;
		},
		first: function () {
			var query = self.squel.select().from(self._tableName(), self.alias).limit(1);
			return query;
		},
		count: function () {
			var query = self.squel.select().field('COUNT(*)', 'count').from(self._tableName(), self.alias);
			return query;
		},
		list: function () {
			var query = self.squel.select().from(self._tableName(), self.alias);
			return query;
		},
		neighbors: function () {
		},
		threaded: function () {
		}
	};
	return callers[type]();
};

AbstractModel.prototype.find = function(type, callback) {
	var query = this._find(type);
	var sql = callback(query);
	// var find = Database.query(sql);
	// return find;
	debug(sql.toString());
	return sql.toString();
};

AbstractModel.prototype.saveField = function(field, value){
	if(!this.id){
		console.log('Warning: No has model id!');
		return;
	}
	var sql = this.squel.update().table(this.useTable).set(field, value).where(this.primaryKey+' = ?', this.id);
	this.id = null;
	debug(sql.toString());
	return sql.toString();
};

AbstractModel.prototype.exists = function(id){
	var sql = this.squel.select().field('COUNT(*)', 'count').from(this.useTable).where(this.primaryKey+' = ?', id);
	debug(sql.toString());
	return sql.toString();
};

AbstractModel.prototype.hasAny = function(callback) {
	var query = this.squel.select().from(this.useTable);
	var sql = callback(query);
	debug(sql.toString());
	return sql.toString();
};

AbstractModel.prototype.field = function(field, callback) {
	var query = this.squel.select().field(field).from(this._tableName(), this.alias);
	var sql = callback(query);
	debug(sql.toString());
	return sql;
};

AbstractModel.prototype.delete = function(id){
	var query = this.squel.delete().from(this._tableName()).where(this.primaryKey+' = ?', id);
	debug(query.toString());
	return query.toString();
};

AbstractModel.prototype.deleteAll = function(callback){
	var query =  this.squel.delete().from(this._tableName(), this.alias);
	var sql = callback(query);
	debug(sql.toString());
	return sql;
};

AbstractModel.prototype.updateAll = function(fields, callback){
	var query =  this.squel.update().table(this._tableName()).setFields(fields);
	var sql = callback(query);
	debug(sql.toString());
	return sql;
};

AbstractModel.prototype.save = function(data){
	var self = this;
	if(self.id){ // Update
		var query = self.updateAll(data, function(squel){
			return squel.where(self.primaryKey+' = ?', self.id);
		});
		return query.toString();
	}else{ // Insert
		var query = self.squel.insert().into(self._tableName()).setFields(data);
		debug(query.toString());
		return query.toString();
	}
};

AbstractModel.prototype.read = function(field, id){
	var query = this.squel.select().from(this._tableName(), this.alias).where(this.primaryKey+' = ?', id);
	if(field){
		query = query.field(field);
	}
	debug(query.toString());
	return query.toString();
};

AbstractModel.prototype.begin = function(){
	var sql = 'BEGIN';
	debug(sql);
	return sql;
};

AbstractModel.prototype.commit = function(){
	var sql = 'COMMIT';
	debug(sql);
	return sql;
};

AbstractModel.prototype.rollback = function(){
	var sql = 'ROLLBACK';
	debug(sql);
	return sql;
};

AbstractModel.prototype.paginate = function(conditions, page, limit){

};

module.exports = AbstractModel;