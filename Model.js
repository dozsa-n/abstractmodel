var root = './';

function Model(){

}

Model.prototype.uses = function(){
    var args = arguments;
    for(var i in args){
        var name = args[i];
        var file = root+name+'Model.js';
        Model.prototype[name] = new (require(file));
    }
};
module.exports = Model;