var Model = new (require('./Model.js'));
Model.uses('User', 'Profile');
// var User = new (require('./UserModel.js'));
// var Profile = new (require('./ProfileModel.js'));

// var User = new User();
var findAllUser = Model.User.find('all', function(squel){
    return squel.where('1 <> 1');
});

// var Profile = new Profile();
var findAllProfile = Model.Profile.find('all', function(squel){
    return squel.where('1 >< 1');
});

Model.User.id = 99;
Model.User.saveField('email', 'dozsa.n@gmail.com');

Model.User.exists(25);

Model.User.field('fullname', function(squel){
    return squel.where('data = ?', 'most');
});

Model.User.delete(22);

Model.User.deleteAll(function(squel){
    return squel.where('data = ?', 'delete');
});

Model.User.updateAll({name: 'asd', age: 22}, function(squel){
    return squel.where('id = ?', 1);
});

Model.User.id = 1;
Model.User.save({
    age: 25,
    name: "Dózsa Norbert",
    email: "dozsa.n@gmail.com",
    gender: 1,
    city: "Budapest",
    zip_code: '1045'
});

Model.User.read('name', 3);